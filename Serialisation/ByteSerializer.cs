using System;
using System.Collections;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using Framework.Serialization.Surrogate;
using UnityEngine;

namespace Framework.Serialization {

    public class BinarySerializer {

        public static byte[] Serialize (System.Object data) {
            
            BinaryFormatter _binary_formatter = new BinaryFormatter ();
            _binary_formatter.SurrogateSelector = GetUnitySurrogate();

            // serialise
            MemoryStream _stream = new MemoryStream();
            _binary_formatter.Serialize(_stream, data);

            return _stream.GetBuffer();
        }

        public static T DeSerialize<T> (byte[] data) {
            
            BinaryFormatter _binary_formatter = new BinaryFormatter ();
            _binary_formatter.SurrogateSelector = GetUnitySurrogate();
            Stream _stream = new MemoryStream(data);
            
            return ((T) _binary_formatter.Deserialize(_stream));
        }

        static SurrogateSelector GetUnitySurrogate()
        {
             // setup surrogate
            SurrogateSelector _surrogate_selector   = new SurrogateSelector ();
            StreamingContext _stream_context        = new StreamingContext (StreamingContextStates.All);

            // vec3 surrogate
            _surrogate_selector.AddSurrogate (typeof (Vector3),
                                                _stream_context,
                                                new Vector3SerializationSurrogate ());
            // quat surrogate
            _surrogate_selector.AddSurrogate (typeof (Quaternion),
                                                _stream_context,
                                                new QuaternionSerializationSurrogate());

            return _surrogate_selector;
        }
    }
}