﻿using System;
using UnityEngine.UI;

namespace Framework.UI
{
    /// <summary>
    /// Inputs Reserved: 0 Title, 1 Message
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="U"></typeparam>
    public class UIDialog<T> : UIDialog<T, object> where T : IConvertible { }

    /// <summary>
    /// Inputs Reserved: 0 Title, 1 Message
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="U"></typeparam>
    public class UIDialog<T, U> : UIElement<T, U> where T : IConvertible
    {
        // editor
        public Text Title;
        public Text Message;

        public override void Show(Action<T, U> response, params object[] input)
        {
            string title = input[0] as string;
            string message = input[1] as string;

            if (Title != null)
                Title.text = title;

            if (Message != null)
                Message.text = message;

            Log("Showing dialog, \n\t\t Title: " + title
                        + "\n\t\t Body: " + message);

            base.Show(response, input);
        }
    }

}
