﻿using System;
using UnityEngine;

namespace Framework.UI
{
    public class UIElement : UIElement<UIElement.Response>
    {
        public enum Response
        {
            NONE
        }

        public void Show()
        {
            Debug.Log("UIElement1");
            Show(null);
        }

        public override void Show(Action<Response, object> response, params object[] input)
        {
            base.Show(response, input);
        }
        public override void Hide()
        {
            Debug.Log("UIElement1.hide");

            base.Hide();
        }
    }
    public class UIElement<T> : UIElement<T, object> where T : IConvertible
    {
        public void Show(Action<T, object> response)
        {
            Show(response, null);
        }

        public override void Show(Action<T, object> response, params object[] input)
        {
            Debug.Log("UIElement2.show");

            base.Show(response, input);
        }
        public override void Hide()
        {
            Debug.Log("UIElement2.hide "+gameObject.name);

            base.Hide();
        }
    }
    public class UIElement<T, U> : MonoBehaviour where T : IConvertible
    {
        public virtual void Show(Action<T, U> response, params object[] input)
        {
            m_response = response;
            gameObject.SetActive(true);
            OnShow(input);
        }
        public virtual void Hide()
        {
            Debug.Log("UIElement3.hide" + gameObject.name);

            gameObject.SetActive(false);
            OnHide();
        }

        // protected
        protected virtual void OnShow(object[] input) {}
        protected virtual void OnHide() {}

        protected void SendResponse(T code, U payload)
        {
            if (m_response != null)
                m_response(code, payload);
        }

        protected void Log(string message)
        {
            Debug.Log("[UI] "+GetType().Name + ": " + message);
        }
        protected void LogError(string message)
        {
            Debug.LogError("[UI] " +GetType().Name + " [UI] : " + message);
        }

        // private
        Action<T, U> m_response;
    }
}