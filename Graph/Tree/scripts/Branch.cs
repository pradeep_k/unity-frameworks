﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using Framework.Extension;

namespace Framework.Graph
{
    public class Branch : Node
    {
        // const
        public const string ERROR_MESSAGE_FIELD_NOT_SET                   = "Field not set.";

        public const string ERROR_MESSAGE_FIELD_DOES_NOT_EXIST            = "Field does not exist";
        public const string ERROR_MESSAGE_NODE_DOES_NOT_EXIST             = "Node does not exist";

        public const string ERROR_MESSAGE_PERMISSIION_DENIED_ADD_NODE     = ERROR_MESSAGE_PERMISSION_DENIED + ". Cannot add node";
        public const string ERROR_MESSAGE_PERMISSIION_DENIED_ADD_FIELD    = ERROR_MESSAGE_PERMISSION_DENIED + ". Cannot add field";

        // callbacks
        public Action<Node> ChildAdded;
        public Action<Node> ChildRemoved;

        // public
        public int ChildCount   { get{ return FieldCount + BranchCount; }}
        public int FieldCount   { get{ return m_fields.Values.Where(x => x.Exists).Count(); }}
        public int BranchCount  { get{ return m_branches.Values.Where(x => x.Exists).Count(); }}

        /// <summary>
        /// Gets the children fields.
        /// </summary>
        /// <value>The fields.</value>
        public List<Field>  Fields      { get { return _GetFields().FindAll(x => x.Exists); } }
        /// <summary>
        /// Gets the children branches.
        /// </summary>
        /// <value>The branches.</value>
        public List<Branch> Branches    { get { return _GetBranches().FindAll(x => x.Exists); } }
        /// <summary>
        /// Gets all children.
        /// </summary>
        /// <value>The children.</value>
        public List<Node>   Children    { get { List<Node> children = new List<Node>();
                                                children.AddRange(Fields.Select(x => (Node)x));
                                                children.AddRange(Branches.Select(x => (Node)x));
                                                return children; }}


        public Branch(Branch parent, string id, string json, Flag flag) : base(parent, id)
        {
            if(!string.IsNullOrEmpty(json))
                Populate(json);

            _Set(this, flag);
        }

        public override bool Populate(string json)
        {
            string[] data = null;

            json = json.TrimStart();
            bool _is_array = json[0] == '[';
            if(_is_array)
            {
                // array data
                json = json.RemoveOuterMost("[", "]").Trim();
                // Log("Populating(Array as fields): "+json); // debug only*
                if (!string.IsNullOrEmpty(json))
                    data = json.SplitJsonToArray();

                if (data != null && data.Length > 0)
                {
                    // debug only*
                    // Log("Populate: \n\t" + (data.Length == 1 ? data[0] : data.Aggregate((x, y) => { return x + ",\n\t" + y; })));
                    int _id = 0;
                    Array.ForEach(data, d =>
                    {
                        // field
                        CreateField(_id.ToString(), d, false);
                        _id++;
                    });
                }
            }
            else
            {
                // node data
                json = json.RemoveOuterMost("{", "}").Trim();
                // Log("Populating: "+json); // debug only*
                if (!string.IsNullOrEmpty(json))
                {
                    data = json.SplitJsonToArray();
                }

                if (data != null && data.Length > 0)
                {
                    // debug only*
                    // Log("Populate: \n\t" + (data.Length == 1 ? data[0] : data.Aggregate((x, y) => { return x + ",\n\t" + y; })));
                    Array.ForEach(data, d =>
                    {
                        string[] _str_array = d.SplitFirstFromString(':');

                        // id
                        string _id;
                        _str_array[0] = _str_array[0].Trim();
                        _id = _str_array[0].RemoveOuterMostQuotes();

                        // data
                        _str_array[1] = _str_array[1].Trim();

                        // is branch
                        if (_str_array[1][0] == '{' || _str_array[1][0] == '[' )
                        {
                            // branch
                            CreateBranch(_id, _str_array[1], false);
                        }
                        else
                        {
                            // field
                            CreateField(_id, _str_array[1], false);
                        }
                    });
                }
            }

            return true;
        }

        public override bool Merge(Node source)
        {
            if (source.GetType() == typeof(Branch))
            {
                if (Exists)
                {
                    if (!source.Exists) // source is marked as removed
                    {
                        // don't propagate any further mark as removed
                        _Set(this, Flag.REMOVED, true);
                    }
                    else
                    {
                        // merge children
                        return MergeFields((source as Branch)._GetFields()) 
                                && MergeBranches((source as Branch)._GetBranches());
                    }
                }
                
                return true;
            }

            LogError("Cannot merge field node with branch node");
            return false;
        }

        // getters
        /// <summary> Gets the node pointed by the url </summary>
		/// <return> Returns Null if node does not exist </return>
        public Node Get(string url)
        {
            if(url == string.Empty)
                return this;

            string[] _path = url.SplitFirstPathItemName();
            if(_path.Length > 1)
            {
                Branch _node = GetBranch(_path[0]);
                return _node != null ? _node.Get(_path[1]) : null;
            }
            else 
            {
                // last point look for endpoint
                Field _field = GetField(_path[0]);
                if (_field != null)
                    return _field;
                else
                    return GetBranch(_path[0]);

            }
        }
        /// <summary> Gets the node pointed by the url </summary>
        /// <return> Returns Null if node does not exist </return>
        /// <param name="generateifnotfound"> Will generate path if it does not exist </param>
        public T Get<T>(string url, bool generateifnotfound=false, bool markdirty = true) where T : Node
        {
            T result = Get(url) as T;
            if(result == null && generateifnotfound)
            {
                Log("Adding path: "+url+"\n\t\tType: "+typeof(T).ToString());
                result = Add<T>(url, markdirty);
            }
                
            return result;
        }
        
        /// <summary> Looks for nodes that match the predicate </summary>
        public List<Node> FindAll(Func<Node, bool> predicate, bool recursively=false)
        {
            return _FindAllNodesThatMatch(predicate, recursively).FindAll(x => x.Exists);
        }

        // add
        /// <summary> Generates a node from path </summary>
        /// <return> Returns Null if permission was denied </return>
        public T Add<T>(string url, bool markdirty = true) where T : Node
        {
            Node _node;
            string[] _path = url.SplitFirstPathItemName();
            if(_path.Length > 1)
            {
                _node = CreateBranch(_path[0], null, markdirty);
                if(_node != null)
                    return (_node as Branch).Add<T>(_path[1], markdirty);
            }
            else 
            {
                // endpoint
                if(typeof(T) == typeof(Field))
                    _node = CreateField(_path[0], null, markdirty);
                else
                    _node = CreateBranch(_path[0], null, markdirty);
            }

            return _node as T;
        }

        public override void GetDirty(ref List<Node> nodes)
        {
            base.GetDirty(ref nodes);

            foreach(var x in _GetBranches())
                x.GetDirty(ref nodes);

            foreach(var y in _GetFields())
                y.GetDirty(ref nodes);
        }


        /// <summary> Removes dirty flag from children </summary>
        public override void ClearDirty()
        {
            base.ClearDirty();
            List<Node> _dirty = GetDirty();
            _dirty.ForEach( y => y._Unset(this,Flag.DIRTY));
        }

        public override string ToJson(bool sortalphabetically=true)
        {
            string _json = "";

            // aggregate
            if(IsArray())
            {
                // array branch
                List<Field> _fields = m_fields.Values.ToList().FindAll(x => x.Exists);
                _fields = _fields.OrderBy(x => x.Id, new SemiNumericComparer()).ToList();
                if (_fields.Count > 0)
                {
                    string _i = _fields.Last().Id;
                    int _max = int.Parse(_fields.Last().Id);

                    Field _f;
                    for (int i = 0; i <= _max; i++)
                    {
                        _f = Get<Field>(i.ToString());

                        if (_f != null && _f.GetValue() != null)
                            _json = _json + _f.GetValue().GetJson();
                        else
                            _json = _json + "null";

                        _json = _json + (i != _max ? "," : "");
                    }

                }
                _json = "[" + _json + "]";
            }
            else
            {
                List<Node> _nodes = new List<Node>();
                // fields
                _nodes.AddRange(m_fields.Values.ToList().FindAll(x => x.Exists && x.GetValue() != null)
                                                        .Cast<Node>());
                // branches
                _nodes.AddRange(m_branches.Values.ToList().FindAll(x => x.Exists).Cast<Node>());

                // sort
                if (sortalphabetically)
                    _nodes = _nodes.OrderBy(x => x.Id, new SemiNumericComparer()).ToList();

                if (_nodes.Count() > 0)
                {
                    _json = (_nodes.Count() < 2 ? _nodes[0].ToJson()
                                                : _nodes.Select(x => x.ToJson())
                                                .Aggregate((x, y) => { return x + "," + y; }));
                }

                _json = "{" + _json + "}";
            }

            return ( !IsRoot ? "\""+Id+"\":" : "" ) +_json ;
        }
        
        // overrides
        public override bool Remove()
        {
            if(!base.Remove())
                return false;

            m_fields.Clear();
            m_branches.Clear();
            return true;
        }

        // hidden
        /// <param name="actiontype"> 1 = Add, -1 = Remove </param>
        public override void _Modified(Node sender, int actiontype=0)
        {
            switch(actiontype)
            {
                // Add
                case 1:
                    if (ChildAdded != null)
                        ChildAdded(sender);
                break;

                // Remove
                case -1:
                    if (ChildRemoved != null)
                        ChildRemoved(sender);
                break;

                default:
                break;
            }

            base._Modified(sender, actiontype);
        }
        /// <remarks> *Don't use Flag.REMOVED unless you know what you are doing. </remarks>
        public override bool _Set(Node sender, Flag flag, bool ignoreremoveifdirty = false, bool markremoveasdirty = true)
        {
            // propogate to children
            Flag _filtered_flag = RemoveFlag(flag, Flag.DIRTY); // dirty flag should not propogate down
            if (_filtered_flag != Flag.NONE)    // avoid empty recursive loop when dirty is set
            {
                // not a merge
                if (!ignoreremoveifdirty)
                {
                    _GetFields().ForEach(f => { f._Set(sender, flag); });
                    _GetBranches().ForEach(f => { f._Set(sender, flag); });
                }
                // merge
                else
                {
                    int failed = 0;

                    _GetFields().ForEach(f =>
                                                {
                                                    if (!f._Set(sender, flag, ignoreremoveifdirty))
                                                    {
                                                        Log("Merge cannot remove node: " + f.ToStringFull() + ". \n\t\t*It is dirty.");
                                                        failed++;
                                                    }
                                                });
                    _GetBranches().ForEach(f =>
                                                    {
                                                        if (!f._Set(sender, flag, ignoreremoveifdirty))
                                                        {
                                                            Log("Merge cannot remove node: " + f.ToStringFull() + ". \n\t\t*It is dirty.");
                                                            failed++;
                                                        }
                                                    });

                    if (CompareFlag(flag, Flag.REMOVED))
                    {
                        if (failed != 0) // has any child failed to remove
                            return false;
                    }
                }
            }

            return base._Set(sender, flag, ignoreremoveifdirty, markremoveasdirty);
        }

        public override bool _Unset(Node sender, Flag flag)
        {
            if (flag != Flag.DIRTY) // dirty flag should not propogate down
            {
                // propogate to children
                _GetFields().ForEach(f => { f._Unset(sender, flag); });
                _GetBranches().ForEach(f => { f._Unset(sender, flag); });
            }

            return base._Unset(sender, flag);
        }

        /// <summary>
        /// Don't use this unless you know what you are doing. Note* this will include remove marked nodes.
        /// </summary>
        public List<Field> _GetFields()
        {
            return m_fields.Values.ToList();
        }

        /// <summary>
        /// Don't use this unless you know what you are doing. Note* this will include remove marked nodes.
        /// </summary>
        public List<Branch> _GetBranches()
        {
            return m_branches.Values.ToList();
        }

        // privates
        Dictionary<string, Field> m_fields    = new Dictionary<string, Field>();
        Dictionary<string, Branch> m_branches = new Dictionary<string, Branch>();

        bool IsArray()
        {
            List<Field> _fields = _GetFields();
            if(_fields.Count == 0)
                return false;
            
            int _val = -1;          
            return BranchCount == 0
                   && _fields.Where(x => { return x.Exists && !int.TryParse(x.Id, out _val); })
                                                    .Count() == 0;
        }

        // getters
        Field GetField(string name)
        {
            Field _field = GetFieldByName(name);
            if(_field != null && _field.Exists)
            {
                if (_field.CanRead)
                {
                    return _field;
                }
                else
                {
                    LogError(ERROR_MESSAGE_CANNOT_READ + " Field: " + name +" ["+_field.GetPermissions()+"]");
                    return null;
                }
            }

            return null;
        }
        Branch GetBranch(string id)
        {
            Branch _node = GetBranchById(id);
            if(_node != null && _node.Exists)
            {
                if (_node.CanRead)
                {
                    return _node;
                }
                else
                {
                    LogError(ERROR_MESSAGE_CANNOT_READ + " Node: " + id +" ["+_node.GetPermissions()+"]");
                    return null;
                }
            }

            return null;
        }

        /// <summary>This will include remove marked and no access fields also </summary>
        Node GetNodeById(string id)
        {
            Node n =  GetFieldByName(id);
            return n != null ? n : GetBranchById(id);
        }

        /// <summary>This will include remove marked and no access fields also </summary>
        Field GetFieldByName(string name)
        {
            return m_fields.ContainsKey(name) ? m_fields[name] : null;
        }
        /// <summary>This will include remove marked and no access fields also </summary>
        Branch GetBranchById(string id)
        {
            return m_branches.ContainsKey(id) ? m_branches[id] : null;
        }
        /// <summary> *Don't use this unless you know what you are doing. *This will include remove flagged nodes.</summary>
        public List<Node> _FindAllNodesThatMatch(Func<Node, bool> predicate, bool recursively=false)
        {
            List<Node> _result = new List<Node>();
            _result.AddRange(_GetBranches().Select( x => ((Node) x)).Where(predicate));
            if(recursively)
                _GetBranches().ForEach( x => { _result.AddRange(((Branch) x)._FindAllNodesThatMatch(predicate, recursively)); } );

            _result.AddRange(_GetFields().Select( x => ((Node) x)).Where(predicate));

            return _result;
        }

        // create
        /// <summary>This will create a new node.</summary>
        Branch CreateBranch(string id, string json, bool markdirty = true, bool ignorepermission = false)
        {
            if(!ignorepermission && !CanWrite)
            {
                LogError(ERROR_MESSAGE_PERMISSIION_DENIED_ADD_NODE+": "+id, true);
                return null;
            }

            Branch node = GetNodeById(id) as Branch;
            Flag _flag = this.GetPermissions();

            // nodes created during populate should not be marked dirty
            _flag = markdirty ? _flag | Flag.DIRTY : _flag;
            if(node == null)
            {   
                // new node
                node = new Branch(this, id, json, _flag);
                m_branches.Add(id, node);

                node.Modified(1);
            }
            else if(node.Exists)
            {
                // node collision
                return node;
            }
            else
            {
                // a node marked for removal already exists
                node = new Branch(this, id, json, _flag);
                m_branches[id] = node;

                node.Modified(1);
            }

            return node;
        }
        /// <summary>This will create a new node.</summary>
        Field CreateField(string name, string json, bool markdirty = true, bool ignorepermission = false)
        {
             if(!ignorepermission && !CanWrite)
            {
                LogError(ERROR_MESSAGE_PERMISSIION_DENIED_ADD_FIELD+": "+name, true);
                return null;
            }

            Field field = GetNodeById(name) as Field;
            Flag _flag = this.GetPermissions();

            // nodes created during populate should not be marked dirty
            _flag = markdirty ? _flag | Flag.DIRTY : _flag;
            if(field == null)
            {
                // new field
                field = new Field(this, name, json, _flag);
                m_fields.Add(name, field);

                field.Modified(1);
            }
            else if(field.Exists)
            {
                // field already exists
                return field;
            }
            else
            {
                // a field marked for removal already exists
                field = new Field(this, name, json, _flag); // replace reference with new one
                m_fields[name] = field;

                field.Modified(1);
            }

            return field;
        }

        // merge
        bool MergeFields(List<Field> src)
        {
            // hard merge
            // fields missing in src
            List<Field> _missing_in_src = _GetFields().FindAll(x => !x.IsDirty // ignore dirty
                                                                    && x.Exists // ignore already removed
                                                                    && (src.Find(y => y.Id == x.Id) == null));
            
            _missing_in_src.ForEach( x => x._Set(this, Flag.REMOVED, true));

            // soft merge
            int _failed = 0;
            foreach (var src_field in src)
            {
                Field _field = GetFieldByName(src_field.Name);
                if (_field != null)
                {
                    if(!_field.Merge(src_field))
                        _failed++;
                }
                else
                {
                    // add a new field
                    CreateField(src_field.Name, src_field.GetValue().GetJson(), false);
                }
            }

            return _failed == 0;
        }

        bool MergeBranches(List<Branch> src)
        {
            // hard merge
            // branches missing in src
            List<Branch> _missing_in_src = _GetBranches().FindAll(x => x.Exists 
                                                                        && (src.Find(y => y.Id == x.Id) == null));

            _missing_in_src.ForEach( x => x._Set(this, Flag.REMOVED, true));

            // soft merge
            int _failed = 0;
            foreach (var src_node in src)
            {
                Branch _branch = GetBranchById(src_node.Id);
                if (_branch != null)
                {
                    // propagate to children
                    if(!_branch.Merge(src_node))
                        _failed++;
                }
                else
                {
                    // append a new node
                    CreateBranch(src_node.Id, src_node.ToJson(), false);
                }
            }
            
            return _failed == 0;
        }
    }    
}