using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using Framework.Extension;
using UnityEngine;

namespace Framework.Graph
{
    [Flags]
    public enum Flag
    {
        NONE = 0,

        // ORDER: *DIRTY | *REMOVED | WRITE | READ 
        // "*" represents internal flag 
        READ     = 1,
        WRITE    = 2,

        // internal flags: these flags will be set/unset automatically and should not be set by user
        REMOVED  = 4,
        /// <summary> Dirty nodes will be ignored during merge. 
        /// *Note: Only valid for target not source i.e if 
        /// source tree is marked as dirty it won't be considered.
        /// Don't set this manually.
        /// </summary>
        /// <remarks> This will reverse propogate to root or 
        /// till a parent node that already is marked dirty </remarks>
        DIRTY    = 8 
    }

    public class Node
    {
        // const
        public const string ERROR_MESSAGE_PERMISSION_DENIED = "Permission Denied";
        public const string ERROR_MESSAGE_CANNOT_READ       = ERROR_MESSAGE_PERMISSION_DENIED + ". Cannot Read";
        public const string ERROR_MESSAGE_CANNOT_WRITE      = ERROR_MESSAGE_PERMISSION_DENIED + ". Cannot Write";
        public const string ERROR_MESSAGE_CANNOT_REMOVE     = ERROR_MESSAGE_PERMISSION_DENIED + ". Cannot remove node";

        public const string ERROR_MESSAGE_INTERNAL_FLAGS_NOT_ALLOWED   = "Internal Flags Not Allowed";


        public const int INTERNAL_FLAG_START = 4;

        // callbacks
        public Action<Node> Updated; // called on all updated node along the path

        public bool IsRoot      { get{ return m_parent == null; }}
        // permission
        public bool CanRead     { get{ return CompareFlag(m_flag, Flag.READ);       }}
        public bool CanWrite    { get{ return CompareFlag(m_flag, Flag.WRITE);      }}
        // states
        public bool Exists      { get{ return !CompareFlag(m_flag, Flag.REMOVED);   }}
        public bool IsDirty     { get{ return CompareFlag(m_flag, Flag.DIRTY);      }} // ignored during merge

        public string   Id      { get{ return m_id; }}   
        public string   Path    { get{ return m_parent != null ? (!string.IsNullOrEmpty(m_parent.Path) ? m_parent.Path + "/"+ Id : Id) 
                                                                    : ""; } }

        public Node Parent      { get{return m_parent; }}

        public Node(Branch parent, string  id, Flag permission = Flag.READ | Flag.WRITE)
        {
            m_id     = id;
            m_parent = parent;
        }

        public void Modified(int actiontype=0)
        {
            _Modified(this, actiontype);
        }

        public Flag GetPermissions()
        {
            return MaskedFlag(m_flag, 0, 1);
        }

        public bool SetFlag(Flag flag)
        {
            if((int) flag > INTERNAL_FLAG_START)
            {
                LogError(ERROR_MESSAGE_INTERNAL_FLAGS_NOT_ALLOWED);
                return false;
            }

            return _Set(this, flag);
        }

        public bool UnsetFlag(Flag flag)
        {
            if((int) flag > INTERNAL_FLAG_START)
            {
                LogError(ERROR_MESSAGE_INTERNAL_FLAGS_NOT_ALLOWED);
                return false;
            }

            return _Unset(this, flag);
        }

        public Flag GetFlag()
        {
            return m_flag;
        }

        public List<Node> GetDirty()
        { 
            List<Node> _nodes = new List<Node>();
            GetDirty(ref _nodes);
            return _nodes;
        }

        // flag
        public static Flag RemoveFlag(Flag source, Flag target)
        {
            return source & ~target;
        }
        public static Flag AddFlag(Flag source, Flag target)
        {
            return source | target;
        }

        public static bool CompareFlag(Flag source, Flag target)
        {
            return (source & target) == target;
        }

        public static Flag MaskedFlag(Flag source, int start, int end )
        {
            return CreateMask(start, end) & source;
        }

        public static Flag CreateMask(int a, int b)
        {
            int r = 0;
            for (int i = a; i <= b; i++)
                r |= 1 << i;

            return (Flag) r;
        }

        // virtuals
        public virtual bool Merge(Node source)      { return false; }
        public virtual bool Populate(string json)   { return false; }
        
        public virtual string ToJson(bool sortalphabetically=true){ return ""; }
        public virtual bool Remove()
        {
            if(!CanWrite)
            {
                LogError(ERROR_MESSAGE_CANNOT_REMOVE);
                return false;
            }
            
            return _Set(this, Flag.REMOVED);
        }
        public virtual void GetDirty(ref List<Node> nodes)
        { 
            if(IsDirty)
                nodes.Add(this);
        }
        public virtual void ClearDirty()
        {
            _Unset(this, Flag.DIRTY);
        }

        // hidden
        /// <param name="actiontype">0 = Default modification. NON-ZEROS will be defined in extended classes</param>
        public virtual void _Modified(Node sender, int actiontype=0)
        {
            if(Updated != null)
                Updated(sender);

            if(m_parent != null)
                m_parent._Modified(sender, actiontype);
        }
        /// <remark> *Don't use unless you know what you are doing. </remark>
        /// <param name="ignoreremoveifdirty"> if dirty remove wont be set, useful during merge. </param>
        /// <param name="markremoveasdirty"> used to ignore dirty setting for removed.</param>
        public virtual bool _Set(Node sender, Flag flag, bool ignoreremoveifdirty=false, bool markremoveasdirty=true)
        {
            // dirty flag should reverse propogate to parent 
            // to avoid losing parent node during destructive/hard merge
            // if parent is dirty no need to progress further assuming that
            // all parents beyond it will already be dirty
            if (CompareFlag(flag, Flag.DIRTY))
            {
                // Log("Marked Dirty"); // debug only*
                if (m_parent != null)
                    m_parent._Set(sender, Flag.DIRTY);
            }
            
            if(CompareFlag(flag, Flag.REMOVED))
            {
                if (ignoreremoveifdirty) // dirty nodes don't get removed during merge
                {
                    if (IsDirty)
                        return false;
                }
                else if(markremoveasdirty)
                {
                    // not a merge
                    // mark removed nodes as dirty
                    _Set(sender, Flag.DIRTY);
                }

                Log("Marked for removal");
                Modified(-1);
            }

            m_flag = AddFlag(m_flag, flag);
            // Log("Flag set to: "+flag); // debug only*
            return true;
        }
        /// <remark> *Don't use unless you know what you are doing. </remark>
        public virtual bool _Unset(Node sender, Flag flag)
        {
            // DON'T REVERSE PROPOGATE DIRTY UNSET. Use branch.ClearDirty();
            m_flag = RemoveFlag(m_flag, flag);
            // Log("Flag unset to: "+flag); // debug only*
            return true;
        }

        // to string
        public override string ToString()
        {
            return "Id: "+Id;
        }

        public string ToStringFull()
        {
            return "Path: ."+Path+" ["+(m_flag == 0 ? "NO ACCESS" : m_flag.ToString())+"]";
        }

        // protected
        protected void Log(string message)
        {
            Debug.Log(FormattedMessage(message));
        }

        protected void LogError(string message, bool showpermission=false)
        {
            Debug.LogError(FormattedMessage(message,showpermission));
        }

        protected string FormattedMessage(string message, bool showpermission=true)
        {
            return GetType().Name +": " + (showpermission ? ToStringFull() : Path ) + "\n\t"+message;
        }

        // private
        string  m_id;
        Branch  m_parent;

        Flag    m_flag = Flag.READ | Flag.WRITE; 
    }
}