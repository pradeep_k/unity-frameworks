using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using Framework.Extension;

namespace Framework.Graph
{
    public class Field : Node
    {
        // const
        public const string ERROR_MESSAGE_SUPPORTED_TYPE = "*Supported Types: bool, long, double, string";

        public string Name { get { return Id; } }

        // constructors
        public Field(Branch parent,string name, string json, Flag flag)  : base(parent, name)
        {
            Initialise(name, json);
            _Set(this, flag);
        }
        public Field(Branch parent,string name, string json)  : base(parent, name)
        {
            Initialise(name, json);
            _Set(this, parent.GetFlag());
        }
        public Field(Branch parent,string name, Flag flag)  : base(parent, name)
        {
            Initialise(name, null);
            _Set(this, flag);
        }

        //publics
        public bool SetValue(object value, bool markdirty=true)
        {
            if(!CanWrite)
            {
                LogError(ERROR_MESSAGE_CANNOT_WRITE +" ["+GetPermissions()+"]");
                return false;
            }

            if(value is object[])
                throw new Exception(FormattedMessage("Object[] Not Allowed. Use array branches. eg. branch.Add<Field>(\"INDICE\") to generate array entries"));
            
            
            if(m_value == null) // if auto generated field
            {
                m_value = value;
                // Log("Value set to: "+m_value); // debug only*
                Modified(0);
            }
            else
            {
                // type check
                if(m_value.GetType() == value.GetType())
                {
                    m_value = value;
                    // Log("Value set to: "+m_value); // debug only*
                    Modified(0);
                }
                else
                    throw new Exception(FormattedMessage("Cannot change type:\n\t\tcurrent type: "+m_value.GetType()
                                                                            +"\n\t\tnew type: "+value.GetType()));
            }

            if(markdirty)
                _Set(this, Flag.DIRTY);

            return true;
        }

        public object GetValue()
        {
            return m_value;
        }

        public override bool Merge(Node source)
        {
            try
            {
                if (source.GetType() == typeof(Field))
                {
                    if (!IsDirty && Exists)
                    {
                        // has this node been removed
                        if (!source.Exists)
                        {
                            // mark as removed
                            _Set(this, Flag.REMOVED, true);
                        }
                        // check type
                        else if ( GetValue() == null // if this was generated on the fly go ahead and merge
                                    || (source as Field).GetValue().GetType() == GetValue().GetType())
                        {
                            object value = (source as Field).GetValue();
                            if (value != m_value)
                            {
                                // update value
                                m_value = value;
                                Modified(0);
                            }
                        }
                        else
                        {
                            LogError("Wrong type. Merge Failed");
                            return false;
                        }
                    }
                }
                return true;
            }
            catch(Exception e)
            {
                LogError("Failed to merge, exception: "+e.Message);
                return false;
            }

            LogError("Cannot merge branch node with field node");
            return false;
        }

        public override string ToJson(bool sortalphabetically=true)
        {
            string _value = "";
            if(m_value == null)
                return _value;

            try
            {
                _value =  m_value.GetJson();
            }
            catch(NotSupportedException)
            {
                throw new NotSupportedException(FormattedMessage("Is of unsupported type " +m_value.GetType() +" "+ERROR_MESSAGE_SUPPORTED_TYPE, false));
            }
            catch(Exception e)
            {
                throw new Exception(FormattedMessage(e.Message));
            }

            return "\"" + Name + "\":" +_value;
        }

        // private
        object m_value;

        void Initialise(string name, string json)
        {
            if(string.IsNullOrEmpty(json))
                return;

            Type _type = json.GetTypeOfJson();
            object _value;
            try
            {
                _value = json.GetJsonAsType(_type);
            }
            catch (NotSupportedException)
            {
                _value = null;
                throw new NotSupportedException(FormattedMessage("Field failed to parse Type: " + _type + "." + Field.ERROR_MESSAGE_SUPPORTED_TYPE, false));
            }
            catch (Exception e)
            {
                _value = null;
                throw new Exception(FormattedMessage("Field failed to parse Type: " + _type + "." + e.Message, false));

            }

            m_value = _value;
        }   
    }
}