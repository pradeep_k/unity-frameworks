﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Framework.Extension;
using UnityEngine;

namespace Framework.Graph
{
    public class Tree
    {
		public Node Root { get{ return root; }}
		public Action<bool> Merged;

		public Tree(string json, Flag permissions = Flag.READ | Flag.WRITE)
		{
			Initialise(json, permissions);
		}
		public Tree(Tree tree, Flag permissions = Flag.READ | Flag.WRITE)
		{
			Initialise(tree.ToJson(), permissions);
		}
		public Tree(Flag permissions = Flag.READ | Flag.WRITE)
		{
			Initialise(null, permissions);
		}

		// getters
		/// <summary> Gets the node pointed by the url. </summary>
		/// <return> Returns Null if node does not exist. </return>
		public Node Get(string url)
		{
			if(url == string.Empty)
                return root;

			return (root as Branch).Get(url);
		}

		/// <summary> Gets the node pointed by the url. </summary>
		/// <return> Returns Null if node does not exist. </return>
        /// <param name="generateifnotfound"> Will generate path if it does not exist. </param>
		public T Get<T>(string url, bool generateifnotfound=false, bool markdirty=true) where T : Node
		{
			return (root as Branch).Get<T>(url, generateifnotfound, markdirty);
		}

		/// <summary> Gets the value pointed by the url. </summary>
		/// <return> Returns "defaultvalue" if value does not exist. </return>
		public object Get(string url, object defaultvalue)
		{
			Field _field = (root as Branch).Get<Field>(url, true, false);
			if(_field.GetValue() == null)
				return defaultvalue;
				
			return _field.GetValue();
		}

		public List<string> GetDirty()
        { 
            List<Node> _nodes = new List<Node>();
            Root.GetDirty(ref _nodes);
            return _nodes.Select(x => x.Path).ToList();
        }

		// setter
		/// <summary> Set field value </summary>
		public bool Set(string url, object value, bool markdirty=true)
		{
			bool success = true;
			try
			{
                success = (root as Branch).Get<Field>(url, true, markdirty).SetValue(value, markdirty);
			}
			catch(Exception e)
			{
				success = false;
				Debug.LogError("Tree: Failed to set, "+(string.IsNullOrEmpty(url) ? "${ROOT}" : url) +"\n\t\tException: "+e.Message+"\n\t\tStack: "+e.StackTrace);
			}

			return success;
		}

		/// <summary> Generates a node using the url </summary>
		/// <return> Returns Null if permission was denied </return>
		public T Add<T>(string url, bool markdirty=true) where T : Node
		{
			return (root as Branch).Add<T>(url, markdirty);
		}

		// predicates
		public List<Node> FindAll(Func<Node, bool> predicate, bool recursively=false)
		{
			return root.GetType() == typeof(Branch) ? (root as Branch).FindAll(predicate, recursively) : null;
		}

		// merge
		public Tree Merge(Tree tree)
		{
			bool success = root.Merge(tree.Root);
			if(Merged != null)
				Merged(success);

			return this;
		}

		// conversions
		public string ToJson()
		{
			return root.ToJson();
		}

		// private
		Node root;

		void Initialise(string json, Flag permissions)
		{
			if((int) permissions > Node.INTERNAL_FLAG_START)
                throw new Exception(Node.ERROR_MESSAGE_INTERNAL_FLAGS_NOT_ALLOWED);
				
			root = new Branch(null, "root", json, permissions);
		}
    }
}