using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using Graph = Framework.Graph; 

namespace Framework.Graph
{
    public class LocalIO
    {
        public const string KEY_DIRTY = "__dirty";

        public Graph.Tree Read(string path, Flag permission=Flag.READ | Flag.WRITE)
        {
            Graph.Tree _tree = null;
            if (File.Exists(path))
            {
                string _str_json = File.ReadAllText(path);
                _tree = new Graph.Tree(_str_json, permission);
                // Debug.Log("LocalIO: Read JSON, \n\t\t"+_tree.ToJson()); // debug only*
                
                Branch _dirty =_tree.Get<Branch>(KEY_DIRTY);
                if(_dirty != null)
                {
                    List<string> _str_dirty_path = _dirty._GetFields().Select(x => x.GetValue() as string).ToList();
                    //Debug.Log("Dirty: \n\t\t"+string.Join("\n\t\t", _str_dirty_path.ToArray())); // debug only*
                    _str_dirty_path.ForEach( x => { 
                                                    Node n = _tree.Get(x); 
                                                    n._Set(n, Flag.DIRTY); 
                                                  });

                    _dirty._Set(null, Flag.REMOVED, false, false);
                }
                return _tree;
            }
            
            return null;
        }

        public Graph.Tree ReadData(string data, Flag permission = Flag.READ | Flag.WRITE)
        {
            Graph.Tree _tree = null;
            string _str_json = data;
            _tree = new Graph.Tree(_str_json, permission);
            // Debug.Log("LocalIO: Read JSON, \n\t\t"+_tree.ToJson()); // debug only*

            Branch _dirty = _tree.Get<Branch>(KEY_DIRTY);
            if (_dirty != null)
            {
                List<string> _str_dirty_path = _dirty._GetFields().Select(x => x.GetValue() as string).ToList();
                //Debug.Log("Dirty: \n\t\t"+string.Join("\n\t\t", _str_dirty_path.ToArray())); // debug only*
                _str_dirty_path.ForEach(x => {
                    Node n = _tree.Get(x);
                    n._Set(n, Flag.DIRTY);
                });

                _dirty._Set(null, Flag.REMOVED, false, false);
            }
            return _tree;

        }

        public bool Write(string path, Tree tree, bool dirtypersistence=false)
        {
            FileInfo fileInfo = new FileInfo(path);
            if (!fileInfo.Directory.Exists)
                fileInfo.Directory.Create();

            if(dirtypersistence)
            {
                List<string> _dirty = GetDirtyNodes(tree.Root).ToList();
                int i = 0;
                _dirty.ForEach(x => { 
                                        tree.Add<Field>(KEY_DIRTY + "/" + i, false).SetValue(x, false);
                                        i++;
                                    });
                //Debug.Log("\n\tDirty:\n\t\t" + string.Join("\n\t\t", _dirty.ToArray())); // debug only*
            }

            //Debug.Log("LocalIO: Writing JSON, \n\t\t"+tree.ToJson()); // debug only*
		    File.WriteAllText (path, tree.ToJson());
            return true;
        }

        public bool Clear(string path)
        {
            if (File.Exists(path))
            {
                File.Delete(path);
                return true;
            }

            return false;
        }

        List<string> GetDirtyNodes(Node node)
        {
            List<string> _dirty = new List<string>();
            if(node.IsDirty)
            {
                List<Node> _nodes = new List<Node>();
                node.GetDirty(ref _nodes);

                _dirty.AddRange(_nodes.Select( x => x.Path ));
            }
            return _dirty;
        }
    }
}