﻿using UnityEngine;

namespace Framework.Extension
{
    public static class TransformExtensions
    {
        public static Transform[] GetChildren(this Transform source)
        {
            Transform[] children = new Transform[source.childCount];
            for (int i = 0; i < children.Length; i++)
                children[i] = source.GetChild(i);

            return children;
        }
    }
}