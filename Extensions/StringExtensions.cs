﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;

namespace Framework.Extension
{
    public static class StringExtensions
    {
        // general
        public static bool IsWhitespace(this string source)
        {
            return (source.Trim ().Length == 0);
        }

        public static string ToTitleCase(this string sentence)
        {
            TextInfo ti = CultureInfo.CurrentCulture.TextInfo;
            sentence = ti.ToTitleCase (sentence.ToLower ());
            return sentence;
        }

        public static string AggregateList(this List<string> strings, string seperator)
        {
            string _result = "";

            if (strings.Count () > 0)
                _result = (strings.Count () < 2 ? strings[0] : strings.Aggregate ((x, y) => { return x + seperator + y; }));

            return _result;
        }

        public static string[] SplitAtPositions(this string source, List<int> delimiterPositions)
        {
            string[] output = new string[delimiterPositions.Count + 1];

            for (int i = 0; i < delimiterPositions.Count; i++)
            {
                int index = i == 0 ? 0 : delimiterPositions[i - 1] + 1;
                int length = delimiterPositions[i] - index;
                string s = source.Substring (index, length);
                output[i] = s;
            }

            string lastString = source.Substring (delimiterPositions.Last () + 1);
            output[output.Length - 1] = lastString;

            return output;
        }

        public static string[] SplitFirstFromString(this string source, char seperator)
        {
            return source.Split (new [] { seperator }, 2);
        }

        public static string RemoveOuterMostQuotes(this string source)
        {
            return source.RemoveOuterMost ("\"", "\"");
        }

        public static string RemoveOuterMost(this string source, string begin, string end)
        {
            int _str_start = source.IndexOf (begin) + 1;
            int _str_end = source.LastIndexOf (end);
            int _str_len = _str_end - _str_start;

            return _str_len > 0 ? source.Substring (_str_start, _str_len) : "";
        }

        public static IEnumerable<int> GetAllIndexes(this string source, string matchString)
        {
            matchString = Regex.Escape (matchString);
            foreach (Match match in Regex.Matches(source, matchString))
            {
                yield return match.Index;
            }
        }

        // Un/Escape
        public static string UnEscapeUnicode(this string value)
        {
            return Regex.Replace (value,
                @"\\[Uu]([0-9A-Fa-f]{4})",
                m => char.ToString ((char) ushort.Parse (m.Groups[1].Value,
                    NumberStyles.AllowHexSpecifier)));
        }

        // path
        public static string[] SplitFirstPathItemName(this string source)
        {
            source = source.Trim ();

            if (source.Length == 0)
                return new string[] { "" };

            if (source[0] == '/')
                source = source.Substring (1);

            return source.SplitFirstFromString ('/');;
        }

        // process keys
        static Dictionary<string, string> m_static_keys;
        public static bool AreProcessKeysReady { get { return m_static_keys != null; } }
		public static void SetProcessableKeys(Dictionary<string,string> dict)
        {
            // merge dict
            foreach (string k in dict.Keys)
                SetProcessableKey (k, dict[k]);
        }
		public static void SetProcessableKey(string key, string value)
		{
            if (m_static_keys == null)
                m_static_keys = new Dictionary<string, string> ();

            if (m_static_keys.ContainsKey (key))
                m_static_keys[key] = value;
            else
                m_static_keys.Add (key, value);

            Debug.Log ("Process Key Set, " + key + " : " + value);
        }

        public static string GetProcessedString(this string source)
        {   
            if (!AreProcessKeysReady)
            {
                Debug.LogError ("StringExtension: GetProcessedString() cannot be used before keys are set.");
                return null;
            }

            return GetProcessedString (source, m_static_keys);
        }

        public static string GetProcessedString(this string source, Dictionary<string, string> keybook)
        {
            string _result = source;
            foreach (string k in keybook.Keys)
                _result = _result.Replace ("${" + k + "}", keybook[k]);

            return _result;
        }

        public static string GetProcessedString(this string source, Dictionary<string, Func<string>> keybook)
        {
            string _result = source;
            foreach (string k in keybook.Keys)
                _result = _result.Replace ("${" + k + "}", keybook[k].Invoke ());

            return _result;
        }

        public static void ResetProcessedString()
        {
            m_static_keys = null;
        }

        // json 
        public static Type GetTypeOfJson(this string value)
        {
            value = value.Trim ();

            // null
            if (value == "null")
                return null;

            // array
            if (value[0] == '[')
                return typeof (object[]);

            // strings
            if (value[0] == '\"')
                return typeof (string);

            // bool
            if (value == "true" || value == "false")
                return typeof (bool);

            return value.GetNumberType ();
        }
        public static object GetJsonAsType(this string jsonvalue, Type type)
        {
            object _value;

            // null
            if(type == null)
            {
                _value = null;
            }

            // array
            else if(type == typeof(object[]))
            {
                string[] _data = jsonvalue.Trim ()
                    .RemoveOuterMost ("[", "]")
                    .SplitJsonToArray ();

                _value = _data.Length > 0 ? _data.Where (i => !string.IsNullOrEmpty (i))
                    .Select (x => x.GetJsonAsType (x.GetTypeOfJson ())).ToArray () : new object[0];
            }

            // bool
            else if (type == typeof(bool))
            {
                _value = jsonvalue == "true";
            }

            // numbers
            else if (type == typeof(int))
            {
                _value = int.Parse (jsonvalue);
            }
            else if (type == typeof(long))
            {
                _value = long.Parse (jsonvalue);
            }
            else if(type == typeof(float))
            {
                _value = float.Parse (jsonvalue);
            }
            else if (type == typeof(double))
            {
                _value = double.Parse (jsonvalue);
            }

            // string
            else if (type == typeof(string))
            {

                _value = jsonvalue.RemoveOuterMostQuotes ()
                    .UnEscapeUnicode ();
            }

            // custom classes
            else
            {
                _value = null;
                throw new NotSupportedException (type.ToString ());
            }

            return _value;
        }

        public static string GetJson(this object value)
        {
            string _value = "";

            // null
            if(value == null)
            {
                _value = "null";
            }

            // array
            else if(value.GetType() == typeof(object[]))
            {
                string json = string.Join (",", ((IEnumerable) value).Cast<object> ()
                    .Select (x => x.GetJson ())
                    .ToArray ());
                _value = "[" + json + "]";
            }

            // bool
            else if (value.GetType() == typeof(bool))
            {
                _value = (bool) value ? "true" : "false";
            }

            // numbers
            else if (value.GetType() == typeof(int))
            {
                _value = value.ToString ();
            }
            else if (value.GetType() == typeof(long))
            {
                _value = ((long) value).ToString ("D11", CultureInfo.InvariantCulture);
            }
            else if (value.GetType() == typeof(float))
            {
                _value = ((float) value).ToString ("F2", CultureInfo.InvariantCulture);
            }
            else if (value.GetType() == typeof(double))
            {
                _value = ((double) value).ToString ("F8", CultureInfo.InvariantCulture);
            }

            // string
            else if (value.GetType() == typeof(string))
            {
                _value = "\"" + value + "\"";
            }

            // custom classes
            else
            {
                throw new NotSupportedException (value.GetType ().ToString ());
            }

            return _value;
        }

        public static string[] SplitJsonToArray(this string source)
        {
            var delimiterPositions = new List<int> ();
            
            int bracesDepth         = 0;
            int bracketsDepth       = 0;

            bool doublequoteFlag    = false;

            if (source.Length > 0)
            {
                for (int i = 0; i < source.Length; i++)
                {
                    switch (source[i])
                    {
                        case '{':
                            bracesDepth++;
                            break;
                        case '}':
                            bracesDepth--;
                            break;
                        case '[':
                            bracketsDepth++;
                            break;
                        case ']':
                            bracketsDepth--;
                            break;
                        case '"':
                            doublequoteFlag = !doublequoteFlag;
                            break;

                        default:
                            if (bracesDepth == 0 && bracketsDepth == 0 && !doublequoteFlag && source[i] == ',')
                            {
                                delimiterPositions.Add (i);
                            }
                            break;
                    }
                }
            }

            return delimiterPositions.Count > 0 ? source.SplitAtPositions (delimiterPositions) : new string[] { source };
        }

        // numbers
        public static Type GetNumberType(this string value)
        {
            // point precision
            // refer: https://docs.microsoft.com/en-us/dotnet/standard/base-types/standard-numeric-format-strings#DFormatString
            int n_type = 0; // 0: no-point, 1: float,: 2 double

            // look for point or exponent notation;
            int _notation_index = value.IndexOf ('.');
            _notation_index = _notation_index == -1 ? value.IndexOf ('e') : _notation_index;
            _notation_index = _notation_index == -1 ? value.IndexOf ('E') : _notation_index;
            if(_notation_index != -1)
            {
                // point notation
                if(value[_notation_index] == '.')
                {
                    // use mantissa to determine type
                    n_type = value.Length - (_notation_index + 1) > 7 ? 2 : 1;
                }
                // 'E'/'e' notation
                else
                {
                    throw new NotSupportedException ("Exponent notation is not supported");
                }
            }

            if (n_type == 0) // no-point
            {
                bool is_32bit = value.Length <= 10;
                if (is_32bit)
                    return typeof (int);
                else
                    return typeof (long);
            }
            if (n_type == 1) // single
                return typeof (float);


            return typeof (double);
        }
    
    public static bool IsNumeric (this string value) {
            try {
                int i = Convert.ToInt32 (value);
                return true;
            } catch (FormatException) {
                return false;
            }
        }
    }

    // comparer
    public class SemiNumericComparer : IComparer<string> {
        public int Compare (string s1, string s2) {
            if (s1.IsNumeric() && s2.IsNumeric()) {
                if (Convert.ToInt32 (s1) > Convert.ToInt32 (s2)) return 1;
                if (Convert.ToInt32 (s1) < Convert.ToInt32 (s2)) return -1;
                if (Convert.ToInt32 (s1) == Convert.ToInt32 (s2)) return 0;
            }

            if (s1.IsNumeric() && !s2.IsNumeric())
                return -1;

            if (!s1.IsNumeric() && s2.IsNumeric())
                return 1;

            return string.Compare (s1, s2, true);
        }
    }
}