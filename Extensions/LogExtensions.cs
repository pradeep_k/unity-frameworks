﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Framework.Extensions
{
    public static class LogExtensions
    {
		public static void Log(this object source, string message)
		{
			Debug.Log(FormattedMessage(source, message));
		}
		public static void LogError(this object source, string message)
		{
			Debug.LogError(FormattedMessage(source, message));
		}

		public static string FormattedMessage(object source, string message)
		{
			return source.GetType().Name+": "+message;
		}
    }
}