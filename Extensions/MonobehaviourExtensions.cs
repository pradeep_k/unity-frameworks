using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Framework.Extension {
    public static class MonobehaviourExtensions {

        /// <summary>
        /// Gets components in children.false *Excludes self
        /// </summary>
        /// <param name="source"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static List<T> GetComponentsInChildrenOnly<T>(this Component source)
        {
            List<T> _components = new List<T>();
            foreach(Transform t in source.transform)
            {
                _components.AddRange(t.GetComponentsInChildren<T>());
            }

            return _components;
        }

        /// <summary>
        /// Looks for component in child. *Stops traversing to further depth on first hit, but continues
        /// in breadth.
        /// </summary>
        /// <param name="source"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static List<T> GetComponentsInChildrenTraverseTillFirstHit<T>(this Component source)
        {
            List<T> _components = new List<T>();
            foreach(Transform t in source.transform)
            {
                T _comp = t.GetComponentInChildren<T>();
                if(_comp != null)
                    _components.Add(_comp);
            }

            return _components;
        }

        public static Coroutine WaitUntil(this MonoBehaviour source, Func<bool> predicate, Action callback)
        {
            return source.StartCoroutine(CRWaitUntil(predicate, callback));
        }

        public static Coroutine WaitAndDoAfterFrame(this MonoBehaviour source, int frame, Action callback)
        {
            return source.StartCoroutine(CRWaitAndDoAfterFrame(frame, callback));
        }

        public static Coroutine WaitAndDo (this MonoBehaviour source, float sec, Action callback) {
            return source.StartCoroutine (CRWaitAndDo (sec, callback));
        }

        // private
        static IEnumerator CRWaitAndDo (float sec, Action callback) {
            yield return new WaitForSeconds (sec);
            if (callback != null)
                callback ();
        }

        static IEnumerator CRWaitAndDoAfterFrame(int frame, Action callback)
        {
            int count = 0;
            while(count < frame)
            {
                yield return null;
                count++;
            }
            
            if (callback != null)
                callback ();
        }

        static IEnumerator CRWaitUntil(Func<bool> predicate, Action callback)
        {
            yield return new WaitUntil(predicate);

            if(callback != null)
                callback();
        }
    }
}