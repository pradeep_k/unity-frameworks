﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;

namespace Framework.Extension
{
    public static class DictionaryExtensions
    {
        public static void AddRange<TKey, TValue>(this Dictionary<TKey, TValue> source, Dictionary<TKey, TValue> collection)
        {
            if (collection == null)
            {
                throw new ArgumentNullException("Collection is null");
            }

            foreach (var item in collection)
            {
                if (!source.ContainsKey(item.Key))
                {
                    source.Add(item.Key, item.Value);
                }
                else
                {
                    source[item.Key] = item.Value;
                }
            }
        }

        public static void RemoveAll<TKey, TValue>(this Dictionary<TKey, TValue> dic, Func<TValue, bool> predicate)
        {
            List<TKey> keysToRemove = new List<TKey>();
            foreach (KeyValuePair<TKey, TValue> keyVal in dic)
            {
                if (predicate(keyVal.Value))
                    keysToRemove.Add(keyVal.Key);
            }

            foreach (TKey key in keysToRemove)
            {
                dic.Remove(key);
            }
        }
    }
}
