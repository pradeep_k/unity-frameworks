using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;

namespace Framework.Extension
{
    public static class BitwiseExtensions
    {
        public static int RemoveBit(this int source, int target)
        {
            return source & ~target;
        }
        public static int AddBit(this int source, int target)
        {
            return source | target;
        }

        public static bool CompareBit(this int source, int target)
        {
            return (source & target) == target;
        }

        public static int MaskedBit(this int source, int start, int end )
        {
            return CreateBitMask(start, end) & source;
        }

        public static int CreateBitMask(int a, int b)
        {
            int r = 0;
            for (int i = a; i <= b; i++)
                r |= 1 << i;

            return r;
        }
    }
}