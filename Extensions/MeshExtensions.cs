using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Framework.Extension {
    using System.Linq;
    using Framework.Graph;
    using Tree = Framework.Graph.Tree;
    
    public static class MeshExtensions {

        public static string ToJson(this Mesh source)
        {
            return ToTree(source).ToJson();
        }

        public static Tree ToTree(this Mesh source)
        {
            Tree tree = new Tree();

            int i = 0;

            string l = "SubMeshCount: "+source.subMeshCount;
            l = l + "\nVertices: "+source.vertices.Length;
            l = l + "\nTriangles: "+source.triangles.Length;

            foreach(var v in  source.vertices)
            {
                tree.Add<Field>("vertices/"+i+"/0").SetValue(v.x);
                tree.Add<Field>("vertices/"+i+"/1").SetValue(v.y);
                tree.Add<Field>("vertices/"+i+"/2").SetValue(v.z);

                i++;
            }

            int j = 0;
            foreach(var tri in  source.triangles)
            {
                tree.Add<Field>("triangles/"+j).SetValue(tri);
                j++;
            }

            // Debug.Log(l);

            return tree;
        }

        public static Mesh ToMesh(this string json)
        {
            return ToMesh(new Tree(json));
        }

        public static Mesh ToMesh(this Tree tree)
        {
            return ToMesh(tree.Root as Branch);
        }

        public static Mesh ToMesh(this Branch branch)
        {
            Mesh _mesh = new Mesh();
            List<Branch> _branches = branch.Get<Branch>("vertices").Branches;
            _mesh.vertices = _branches.Select( x => { return new Vector3((float) x.Get<Field>("0").GetValue(),
                                                                        (float) x.Get<Field>("1").GetValue(),
                                                                        (float) x.Get<Field>("2").GetValue()); }).ToArray();
            _mesh.triangles = branch.Get<Branch>("triangles").Fields.Select( x => { return (int) x.GetValue(); }).ToArray();
            _mesh.RecalculateNormals();
                                    
            return _mesh;
        }
    }
}