﻿using System;
using Framework.Extension;
using Framework.UI;
using UnityEngine;

namespace Framework.FSM.UI
{
    public class UXState : UXState<UIElement.Response>
    {
        // protected implementation. Will never be called for this type
        protected override sealed void OnResponse(UIElement.Response code) { }
    }
    /// <summary>
    /// base.OnEnter() should be called if it is overriden
    /// </summary>
    public class UXState<T> : UXState<T, object> where T : IConvertible
    {
        protected virtual void OnResponse(T code) { }

        // protected implementation
        protected override sealed void OnResponse(T code, object data)
        {
            OnResponse(code);
        }
    }

    /// <summary>
    /// base.OnEnter() should be called if it is overriden
    /// </summary>
    public class UXState<T, U> : State where T : IConvertible
    {
        // editor
        [Tooltip("Only UIElement (Derived) allowed")]
        [SerializeField]
        MonoBehaviour m_UIElement;

        /// <summary>
        /// Will re-use previous inputs incase null is passed
        /// </summary>
        public bool CacheInput = true;

        public void ClearInputCache()
        {
            m_prev_input = null;
        }

        // protected
        protected object[] Input { get { return m_prev_input; } }

        /// <summary>
        /// Changes the ui input before passing to ui. 
        /// *Works only if called inside OnPrepare
        /// </summary>
        /// <param name="inputs">Inputs.</param>
        protected void SetUIInput(params object[] inputs)
        {
            m_ui_input = inputs;
        }

        protected virtual void OnPrepare(object[] inputs) { }
        protected virtual void OnCleanup() { }
        protected virtual void OnResponse(T code, U data) { }


        // protected implementation
        protected override sealed void OnEnter(object[] inputs)
        {
            if (inputs != null)
                m_prev_input = inputs;


            OnPrepare(inputs);
            if(verbose) Log("Prepared");

            // avoid racing in same frame with Show()
            this.WaitAndDoAfterFrame(1, () => {
                if (m_element != null)
                {
                    if(verbose) Log("UI Show called on: "+m_element);
                    m_element.Show(OnResponse, m_ui_input);
                }
            });
            

        }

        protected override sealed void OnExit()
        {
            if (!CacheInput)
                ClearInputCache();

            OnCleanup();
            if(m_element != null)
                m_element.Hide();
        }

        // private
        object[] m_prev_input;
        object[] m_ui_input; // injectable input feed

        UIElement<T, U> m_element { get { return m_UIElement != null ? m_UIElement.GetComponent<UIElement<T, U>>() : null; } }
    }
}