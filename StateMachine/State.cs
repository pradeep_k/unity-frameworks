﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Framework.FSM
{
    public abstract class State : MonoBehaviour
    {
        //editor
        [SerializeField]
        List<State> m_states; // connected states

        // public
        public string ID            { get { return gameObject.name; }}
        public bool IsInitialised   { get { return m_machine != null;  }}

        public void Go(string id, params object[] inputs)
        {
            State state = m_states.Find(x => x.ID == id);
            if(state != null)
            {
                _Exit();
                state._Enter(inputs);
                return;
            }

            LogError("Unknown State, "+id);
        }
        public override string ToString()
        {
            return  ID+" ("+GetType().Name +")"; 
        }

        // protected
        protected bool verbose  { get { return m_machine.verbose; }}
        protected StateMachine machine { get { return m_machine; }}

        protected abstract void OnEnter(object[] inputs);
        protected virtual void OnExit(){}

        protected void Log(string message)
        {
            Debug.Log(FormattedMessage(message));
        }
        protected void LogError(string message)
        {
            Debug.LogError(FormattedMessage(message));
        }
        protected string FormattedMessage(string message)
        {
            return machine.LogPrefix +" "+GetType().Name + ": " + message;
        }

        // internal
        internal void _Init(StateMachine machine)
        {
            if (IsInitialised)
            {
                if (m_machine != machine)
                    throw new Exception(FormattedMessage("State already initialised"));

                return; // Ignore cyclical call
            }

            m_machine = machine;
            if (verbose) Log("Initialised");
            m_states.ForEach(x => x._Init(m_machine));
        }
        internal void _DeInit()
        {
            if (!IsInitialised)
                return;
            
            bool is_verbose = verbose; 
            m_machine = null;

            m_states.ForEach(x => x._DeInit());
            if (is_verbose) Log("De-Initialised");
        }
        internal void _Enter(object[] inputs)
        {
            Log("Entered");
            m_machine._SetCurrentState(this);
            OnEnter(inputs);
        }
        internal void _Exit()
        {
            if (verbose) Log("Exited");
            OnExit();
        }

        // private
        StateMachine m_machine;
    }
}
