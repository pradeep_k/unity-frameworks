﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Framework.FSM
{
    public class StateMachine
    {
        // public
        public bool verbose = false;
        public string LogPrefix = "";
        public State CurrentState { get; private set; }

        public StateMachine(State state)
        {
            try
            {
                state._Init(this);
                m_Entry = state;
            }
            catch(Exception e)
            {
                LogError("Failed to initialise properly. *All states will be de-initialised"
                               + "\n\t\tException: " + e.Message
                               + "\n\t\tStack: " + e.StackTrace);
                m_Entry._DeInit(); // clean up
                return;
            }

            Log("Initialised successfully"
                      +"\n\t\tEntry: "+m_Entry);
        }

        public void Start(params object[] input)
        {
            if(CurrentState != null 
               && m_Entry != CurrentState)
            {
                throw new Exception(FormattedMessage("Should not be started twice"));
            }

            m_Entry._Enter(input);
        }

        public void Kill()
        {
            try
            {
                CurrentState._Exit();
                m_Entry._DeInit();
            }
            catch (Exception e)
            {
                LogError(LogPrefix + " StateMachine: Failed to de-initialise properly."
                               + "\n\t\tException: " + e.Message
                               + "\n\t\tStack: " + e.StackTrace);
                return;
            }
            Log(LogPrefix + " StateMachine: De-Initialised successfully");
        }

        // protected
        protected void Log(string message)
        {
            Debug.Log(FormattedMessage(message));
        }
        protected void LogError(string message)
        {
            Debug.LogError(FormattedMessage(message));
        }
        protected string FormattedMessage(string message)
        {
            return LogPrefix + " StateMachine : " + message;
        }

        // internal
        internal void _SetCurrentState(State state)
        {
            CurrentState = state;
            Log("State changed to " + state);
        }


        // private 
        State m_Entry;

    }
}
