﻿using System.Collections;
using System.Collections.Generic;
using Framework.FSM;
using UnityEngine;

namespace Framework.FSM.Test
{
    public class StateProtein : State
    {
        protected override void OnEnter(object[] inputs)
        {
            Go("StateDietConfirmation", "Proteins are good for muscles");
        }
    }
}
