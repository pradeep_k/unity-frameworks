﻿using System.Collections;
using System.Collections.Generic;
using Framework.FSM;
using UnityEngine;

namespace Framework.FSM.Test
{
    public class StateDietSelection : State
    {
        public enum Diet
        {
            FRUITS,
            PROTIENS
        }

        protected override void OnEnter(object[] inputs)
        {
            Diet _diet = (Diet) inputs[0];
            Go(m_state_switcher[_diet]);
        }

        // private
        Dictionary<Diet, string> m_state_switcher = new Dictionary<Diet, string>()
        {
            {Diet.FRUITS,   "StateFruit"},
            {Diet.PROTIENS, "StateProtein"}
        };
    }
}
