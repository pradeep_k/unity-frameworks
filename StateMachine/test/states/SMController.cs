﻿using System.Collections;
using System.Collections.Generic;
using Framework.FSM;
using UnityEngine;

namespace Framework.FSM.Test
{
    public class SMController : MonoBehaviour
    {
        // editor
        public bool m_verbose;
        public State m_entry;
        public StateDietSelection.Diet m_diet;

        [ContextMenu("Start")]
        public void StartMachine()
        {
            StateMachine machine = new StateMachine(m_entry);
            machine.verbose = m_verbose;
            machine.Start(m_diet);
            machine.Kill();
        }
    }
}
