﻿using System.Collections;
using System.Collections.Generic;
using Framework.FSM;
using UnityEngine;

namespace Framework.FSM.Test
{
    public class StateDietConfirmation : State
    {
        protected override void OnEnter(object[] inputs)
        {
            string message = inputs[0] as string;
            Log("Diet Message: " + message);
        }
    }
}
