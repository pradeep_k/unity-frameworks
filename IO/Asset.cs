using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;

namespace Framework.IO {

    public class Asset : MonoBehaviour {

        public static Asset Instance {
            get {

                if (m_instance == null)
                    m_instance = new GameObject ("Framework.IO.Asset")
                    .AddComponent<Asset> ();

                return m_instance;
            }
        }
        static Asset m_instance;

        // publics
        public void GetStreamingAsset (string filePath, Action<byte[]> callback) {

#if UNITY_EDITOR
            string path = "file://" + Path.Combine (Application.streamingAssetsPath, filePath);
#else
            string path = Path.Combine (Application.streamingAssetsPath, filePath);
#endif
            Get(path, callback);
        }

        /// <param name="uri"> Absolute path to asset</param>
        public Coroutine Get (string uri, Action<byte[]> callback) {
            return StartCoroutine (CRGetStreamingAsset (uri, callback));
        }

        // private
        IEnumerator CRGetStreamingAsset (string uri, Action<byte[]> done) {
            using (UnityWebRequest request = UnityWebRequest.Get (uri)) {
                yield return request.SendWebRequest ();
                if (request.isNetworkError || request.isHttpError) {
                    // handle failure
                    Debug.LogError (request.error);
                    if (done != null)
                        done (null);
                } else {

                    if (done != null)
                        done (request.downloadHandler.data);
                }
            }
        }
    }
}