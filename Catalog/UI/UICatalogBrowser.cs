using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Framework.Extension;

namespace Framework.Catalog {
	[RequireComponent (typeof (UICatalogKey))]
	[RequireComponent (typeof(UnityEngine.UI.ToggleGroup))]
	public class UICatalogBrowser : MonoBehaviour, ICatalogComponent {

		// editor
		public bool Verbose = false;
		public Type type = Type.RADIO;
		public string Prefab;
		public Transform Parent;
		public string[] ExcludeKeys = new string[]{"Property"};
		
		public BrowserEvent Selected;

        void Awake()
        {
            if (Selected == null)
                Selected = new BrowserEvent();
        }

		public void Highlight(string key)
		{
			int index = FindIndexOf(key);
			if(index != -1)
				Highlight(index);
		}

		public void Highlight(int index)
		{
			StopAllCoroutines();
			StartCoroutine(CoRoutineHighlight(index));
		}

		public void Refresh () {
			
			string _json_struct_path = CurrentKeyComponent.FullPath;
			string _catalog_location = GetComponentInParent<UICatalogLocation>().Path;
			List<string> _keys = CatalogManager.Instance.GetKeys(_catalog_location, _json_struct_path, ExcludeKeys);
			if(_keys == null)
			{
				Debug.LogError("UICatalogBrowser: Not found in json, "+_json_struct_path);
				return;
			}

			// remove old
			foreach(Transform child in ParentTransform)
			{
				child.GetComponent<UICatalogKey>().Release();
			}

			// are there items in the current node
			bool items_available = _keys.Count > 0;
			if(!items_available)
				return;

			// add new
			string _log = "";
			bool _is_radio = type == Type.RADIO;

			string prefab_path = CurrentKeyComponent.GetProcessedKey(Prefab);

			ToggleGroup group = GetComponent<ToggleGroup>();
			_keys.ForEach( x => {
				UICatalogKey item = (Instantiate(Resources.Load(prefab_path), 
												ParentTransform) as GameObject)
												.GetComponent<UICatalogKey>();
				item.Key = x;
				item.GetComponent<UICatalogSelectable>().Init(_is_radio);

				Toggle toggle = item.GetComponent<Toggle>();
				toggle.isOn = false;
				toggle.group = group;

				_log = _log + "\n\t\t" +x;

				});

			// highlight the first item
			if(_is_radio)
			{
				Highlight(0);
			}
	
			if(Verbose) Debug.Log("UICatalogBrowser: Instantiated, "+_log);
		}

		internal void OnSelect(UICatalogSelectable selectable)
		{
			// reset previously selected
			if(m_current_selected != null)
				m_current_selected.Reset();

			m_current_selected = selectable;
			if(Verbose) Debug.Log("UICatalogBrowser: Selected, "+m_current_selected);
			if(Selected != null)
                Selected.Invoke(this, m_current_selected.GetComponent<UICatalogKey>().Key);
		}

		internal void OnDeSelect(UICatalogSelectable selectable)
		{
		}

		// private
		UICatalogSelectable m_current_selected;
		UICatalogKey CurrentKeyComponent{ get{ return GetComponent<UICatalogKey> (); }}
		Transform ParentTransform { get{ return Parent != null ? Parent : transform; }} // use self if none specified

		void OnEnable () {
			Refresh();
		}

		/// <param name="key"></param>
		/// <returns>Gameobject index. </returns>
		/// <returns>-1 if no gameobject with key is found </returns>
		int FindIndexOf(string key)
		{
			int index = 0;
			foreach(Transform t in ParentTransform)
			{
				if(t.GetComponent<UICatalogKey>().Key == key)
					return index;

				index++;
			}

			return -1;
		}

		IEnumerator CoRoutineHighlight(int index)
		{
			yield return new WaitForEndOfFrame();
			ParentTransform.GetChild(index).GetComponentInChildren<UICatalogSelectable>().Highlight();
			if(Verbose) Debug.Log("UICatalogBrowser: Highlight called on, "+ParentTransform.GetChild(index).GetComponent<UICatalogKey>().Key);
		}

		[ContextMenu("Test")]
		public void Test()
		{
			
		}
		
		// nested
		public enum Type{
			// CHECKBOX, // TODO
			RADIO
		}

		[System.Serializable]
		public class BrowserEvent : UnityEvent<UICatalogBrowser, string>{}	

		[System.Serializable]
		public class StringListEvent : UnityEvent<List<string>>{} // TODO	// for submit event
	}
}