using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Framework.Catalog {
	[RequireComponent(typeof(UICatalogKey))]
	public class UICatalogLocation : MonoBehaviour, ICatalogComponent {
        
		// editor
		[SerializeField]
		string m_Path;

		public string Path { 	get{ return m_Path; }
								set{ 
									 m_Path = value;
									 Debug.Log("UICatalogLocation: set, "+m_Path);
									 GetComponent<UICatalogKey>().Refresh();
									}}
		
		public void Refresh()
        {
			// do nothing
		}
	}
}