using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Framework.Catalog {
	[RequireComponent (typeof (UICatalogKey))]
	[RequireComponent (typeof (UnityEngine.UI.RawImage))]
	public class UICatalogImage : MonoBehaviour, ICatalogComponent {

		// editor
		public bool Verbose = false;

		public void Refresh () {

			string _json_struct_path = GetComponent<UICatalogKey> ().FullPath;
			string _catalog_location = GetComponentInParent<UICatalogLocation>().Path;
			string _tex_asset_path = CatalogManager.Instance.GetValue (_catalog_location, _json_struct_path) as string;
			if(string.IsNullOrEmpty(_tex_asset_path))
			{
				Debug.LogError("UICatalogImage: Not found in json, "+_json_struct_path);
				return;
			}

			// load texture from resource folder
			Texture2D _tex = Resources.Load (_tex_asset_path) as Texture2D;
			GetComponent<RawImage> ().texture = _tex;

			if(Verbose) Debug.Log("UICatalogImage: Loaded "+_json_struct_path
						+"\n\t\t Tex Path: "+_tex_asset_path
						+"\n\t\t Exists: "+(_tex != null));
		}

		// private
		void Start () {
			Refresh ();
		}

		[ContextMenu ("Test")]
		public void Test () {
			Refresh ();
		}
	}
}