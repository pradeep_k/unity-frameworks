using System;
using System.Collections;
using System.Collections.Generic;
using Framework.Extension;
using UnityEngine;

namespace Framework.Catalog {

	public class UICatalogKey : MonoBehaviour, ICatalogComponent {

		// editor
		[SerializeField]
		string m_key;

		public bool Verbose=false;

		public string Key {
			get { return GetProcessedKey (m_key); }
			set {
				if (Verbose)
					Debug.Log ("UICatalogKey: Updated" +
						"\n\t\t Old: " + m_key +
						"\n\t\t New: " + value);
				m_key = value;
				Refresh ();
			}
		}

		public string FullPath {
			get {
				
				// does this key represent a json file, i.e. all the children components will point to this file
				bool _is_doc_root = GetComponent<UICatalogLocation> () != null;
				if (!_is_doc_root) {

					UICatalogKey parent_key_component = ParentKeyComponent;
					if(parent_key_component == null )
					{
					// if we reach top most catalog key component and still no location component
					throw new Exception("UICatalogKey: No location component found. Check setup in editor");
					}

					string full_path = parent_key_component.FullPath;
					if (string.IsNullOrEmpty (full_path))
						return Key; // don't append "/"

					return parent_key_component.FullPath + "/" + Key; // recursively (up) find full path
				}

				return Key;
			}
		}

		public UICatalogKey ParentKeyComponent {
			get {
				return transform.parent != null ?
					transform.parent.GetComponentInParent<UICatalogKey> () :
					null;
			}
		}

		/// <summary>
		/// Full path prefixed with catalog location
		/// </summary>
		/// <value></value>
		public string WholePath{ get{ return CatalogLocation +"/"+FullPath; }}

		/// <summary>
		/// Catalog location on storage
		/// </summary>
		/// <typeparam name="UICatalogLocation"></typeparam>
		/// <returns></returns>
		public string CatalogLocation{ get{return GetComponentInParent<UICatalogLocation> ().Path; }}

		/// <summary>
		/// Resolves keywords relative to this components key path
		/// </summary>
		/// <param name="key"></param>
		/// <returns></returns>
		public string GetProcessedKey (string key) {
			// Type resolving
			// recursively resolve type keys until absolute key is found
			int key_type = GetKeyType (key);
			string processed_key = key;
			while (key_type != 0) {

				// value type
				if (key_type == 1) {
					processed_key = processed_key.Substring (7); // extract just data
					processed_key = (string) CatalogManager.Instance.GetValue (	CatalogLocation,
																				processed_key);
				}

				key_type = GetKeyType (processed_key);
			}

			// Process catalog keys *CAUSES STACK OVER FLOW
			// Dictionary<string, Func<string>> m_keybook = new Dictionary<string, Func<string>>(){
			// { 	"CURRENT_KEY", 	() => { return m_key; 		}},
			// {	"FULL_PATH", 	() => { return FullPath;	}}

			//  *REMOVED* 
			// TODO should use ".." convention instead of processible key to traverse to parent (should be implemented in Tree class)
			//{	"PARENT_KEY", () => { return transform.parent != null ? transform.parent.GetComponentInParent<UICatalogKey> ().Key : ""; } } 
			// };
			// processed_key = (string) CatalogManager.Instance.GetValue(processed_key.GetProcessedString(m_keybook));

			return processed_key;
		}

		public void Refresh () {

			// if has been released
			if (m_is_released)
				return;

			string new_whole_path = WholePath;
			// ignore if no new update to path
			if (m_prev_whole_path == new_whole_path) {
				if (Verbose)
					Debug.Log ("UICatalogKey: Refresh ignored, nothing new to update" +
						"\n\t\t WholePath: " + m_prev_whole_path);
			} else {
				if (Verbose)
					Debug.Log ("UICatalogKey: Refreshed" +
						"\n\t\t Old: " + m_prev_whole_path +
						"\n\t\t New: " + new_whole_path);

				m_prev_whole_path = new_whole_path;

				// update all sibling ICatalog Components on self
				Array.ForEach (GetComponents<ICatalogComponent> (),
					x => {
						if (((MonoBehaviour) x) == this) {
							// Debug.Log ("UICatalogKey: Found Self, "+new_path); // Debug Only
							return;
						}

						x.Refresh ();
					});

				List<UICatalogKey> key_components = this.GetComponentsInChildrenTraverseTillFirstHit<UICatalogKey> ();
				if (key_components != null)
					key_components.ForEach (
						x => { x.Refresh (); }
					);

			}
		}

		public void Release () {
			m_is_released = true;
			Destroy (gameObject);
		}

		// private
		string m_prev_whole_path;
		bool m_is_released;

		/// <returns>0: Normal json key. </returns>		
		/// <returns>1: value type i.e. @value. </returns>
		int GetKeyType (string key) {
			int type = 0;
			if (key.StartsWith ("@value:"))
				type = 1;
			else if (key.StartsWith ("@json:"))
				type = 2;

			return type;
		}
	}

}