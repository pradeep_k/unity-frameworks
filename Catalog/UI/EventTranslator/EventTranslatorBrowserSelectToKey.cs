using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Framework.Catalog {

[RequireComponent (typeof (UICatalogBrowser))]
public class EventTranslatorBrowserSelectToKey : MonoBehaviour {

	// editor
	public StringEvent Selected;
	public bool FullPath;
	
	void Start()
	{
		GetComponent<UICatalogBrowser>().Selected
									.AddListener((b, key) => 
												{
													if(Selected != null)
													{
														string key_path = FullPath ? b.GetComponent<UICatalogKey>().FullPath +"/" +key : key;
														Selected.Invoke(key_path);
													}
												});
	}

	[System.Serializable]
	public class StringEvent : UnityEvent<string>{}
}
}
