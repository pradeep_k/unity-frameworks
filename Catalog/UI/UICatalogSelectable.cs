using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Framework.Catalog {
	[RequireComponent (typeof (UICatalogKey))]
	[RequireComponent (typeof (UnityEngine.UI.Toggle))]
	public class UICatalogSelectable : MonoBehaviour, ICatalogComponent {

		public void Highlight()
		{
			m_highlight_in_progress = true;
			
			toggle.isOn = true;
			if(toggle.graphic != null)
				toggle.graphic.gameObject.SetActive(true);

			m_highlight_in_progress = false;
		}


		public void Reset()
		{
			toggle.isOn = false;
			toggle.interactable = true;
		}

		public void Init(bool radio)
		{
			m_is_radio = radio;
		}

		public void Refresh () {}

		// privates	
		Toggle toggle { get{ return GetComponent<Toggle>(); }}
		bool m_highlight_in_progress;
		bool m_is_radio = false;

		void Start () {
			toggle.onValueChanged.AddListener(OnSelect);
		}

		void OnSelect(bool selected)
		{
			if(m_highlight_in_progress)
				return;

			if(m_is_radio == true && selected)
			{
				toggle.interactable = false;
			}

			UICatalogBrowser browser = GetComponentInParent<UICatalogBrowser>();
			if(browser != null)
			{
				if(selected)
					browser.OnSelect(this);
				else
					browser.OnDeSelect(this);
			}
		}

		[ContextMenu ("Test")]
		public void Test () {
			Refresh ();
		}
	}
}