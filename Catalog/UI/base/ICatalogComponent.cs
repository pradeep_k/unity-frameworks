namespace Framework.Catalog {
    public interface ICatalogComponent 
    {
        void Refresh();
    }
}