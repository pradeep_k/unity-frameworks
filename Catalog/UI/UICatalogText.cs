using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Framework.Catalog {
	[RequireComponent(typeof(UICatalogKey))]
	[RequireComponent(typeof(UnityEngine.UI.Text))]
	public class UICatalogText : MonoBehaviour, ICatalogComponent {

		// editor
		public bool Verbose;

		public void Refresh()
		{
			string _json_struct_path = GetComponent<UICatalogKey>().FullPath;
			string _catalog_location = GetComponentInParent<UICatalogLocation>().Path;
			string _text = CatalogManager.Instance.GetValue(_catalog_location, _json_struct_path) as string;
			if(_text == null)
			{
				Debug.LogError("UICatalogText: Not found in json, "+_json_struct_path);
				return;
			}

			GetComponent<Text>().text = _text;

			if(Verbose) Debug.Log("UICatalogText: Loaded "+_json_struct_path
					+"\n\t\t Value: "+_text);
		}

		// private
		void Start()
		{
			Refresh();
		}

		[ContextMenu("Test")]
		public void Test()
		{
			Refresh();
		}
	}
}