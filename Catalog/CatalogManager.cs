using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Framework.Graph;
using UnityEngine;

namespace Framework.Catalog {

	using Tree = Graph.Tree;

	public class CatalogManager : MonoBehaviour {

		// editor
		public bool Verbose = false;

		public static CatalogManager Instance{	get{ if(m_instance == null){ m_instance = new GameObject("CatalogManager").AddComponent<CatalogManager>(); } 
													return m_instance;}}

		public object GetValue (string path, string key_path) {
			object result = null;
			try {
				result = (GetTree(path).Root as Branch).Get<Field> (key_path).GetValue ();
			} catch (NullReferenceException e) {
				if (Verbose) Debug.LogError ("CatalogManager: Value not found, " + path +"/"+ key_path);
			}

			return result;
		}

		public List<string> GetKeys (string path, string key_path, string[] exclude_keys) {
			List<string> result = null;
			try {
				result = (GetTree(path).Root as Branch).Get<Branch> (key_path).Branches
																		 .Where(x => { return !exclude_keys.Contains(x.Id); })
																		 .Select(x => x.Id).ToList ();
			} catch (NullReferenceException e) {
				if (Verbose) Debug.LogError ("CatalogManager: Branch not found, " + path +"/"+ key_path);
			}
			return result;
		}

		// private
        private static CatalogManager m_instance;
		Dictionary<string, Tree> m_dict = new Dictionary<string, Tree> ();

        [ContextMenu ("Test")]
		void Test () {
			// Debug.Log (GetValue ("Chair/ROSEWOOD_00/Name"));
		}

		Tree GetTree (string catalog_location) {

			if (!m_dict.ContainsKey (catalog_location)) {

				TextAsset data = Resources.Load<TextAsset> (catalog_location);
				Tree _tree = new LocalIO ().ReadData (data.text);
				if (_tree != null)
				{
					Debug.Log ("CatalogManager: Loaded " + catalog_location);
					m_dict.Add(catalog_location, _tree);
				}
				else
				{
					Debug.LogError ("CatalogManager: Failed to load " + catalog_location);
					return null;
				}
			}
			return m_dict[catalog_location];
		}
	}

}