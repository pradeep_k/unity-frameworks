﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class CameraFeedInjection : MonoBehaviour {
		
		// editor
		public Texture m_InputTex;
		public CameraEvent m_CameraEvent = CameraEvent.BeforeForwardOpaque;

		public void SetInput(Texture input)
		{
			DeInitCommandBuffer();
			m_InputTex = input;
			InitializeCommandBuffer();
		}

        CommandBuffer m_video_command_buffer;

		void Start()
		{
			InitializeCommandBuffer();
		}

		void OnDestroy()
		{
			DeInitCommandBuffer();
		}

		void InitializeCommandBuffer()
		{
			m_video_command_buffer = new CommandBuffer();
			m_video_command_buffer.Blit(m_InputTex, BuiltinRenderTextureType.CurrentActive);

			GetComponent<Camera>().AddCommandBuffer(m_CameraEvent, m_video_command_buffer);			
		}

		void DeInitCommandBuffer()
		{
			if (m_video_command_buffer != null) {
				GetComponent<Camera>().RemoveCommandBuffer(m_CameraEvent, m_video_command_buffer);
			}
		}
}
