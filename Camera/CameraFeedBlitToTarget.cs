﻿using System;
using System.Collections;
using System.Collections.Generic;
using Framework.Extensions;
using UnityEngine;

namespace Framework.Camera
{
    using Camera = UnityEngine.Camera;
    [RequireComponent(typeof(Camera))]
	public class CameraFeedBlitToTarget : MonoBehaviour
    {
        // editor
        public RenderTexture RenderTarget;

        // private
        float m_normalised_aspect_ratio;

        Camera m_camera;
        Rect m_camera_rect;

        ScreenOrientation m_orientation;

        float m_normalised_h { get{ return ((float) Screen.height)/((float) RenderTarget.height);   }}
        float m_normalised_w { get{ return ((float) Screen.width)/((float) RenderTarget.width);     }}
        
        void OnEnable()
        {
            m_camera = GetComponent<Camera>();
            m_camera_rect = new Rect();
        }

        void OnRenderImage(RenderTexture src, RenderTexture trgt)
        {
            Graphics.Blit(src, RenderTarget);
        }

        void Update()
        {
            m_normalised_aspect_ratio = m_normalised_w/m_normalised_h;
            m_orientation = m_normalised_aspect_ratio > 1.00f ? ScreenOrientation.Landscape : ScreenOrientation.Portrait;

            m_camera_rect.xMax = m_orientation == ScreenOrientation.Landscape ? (1/m_normalised_aspect_ratio) : 1;
            m_camera_rect.yMax = m_orientation != ScreenOrientation.Landscape ? m_normalised_aspect_ratio : 1;

            m_camera.rect = m_camera_rect;
        }

    }
}
